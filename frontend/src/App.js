import React, { useEffect, useState } from 'react';
import { ethers } from 'ethers';
import { networks } from './utils/networks';
import './styles/App.css';
import polygonLogo from './assets/polygonlogo.png';
import ethLogo from './assets/ethlogo.png';
import contractAbi from './utils/contractABI.json';

const TLD = '.brian';
const CONTRACT_ADDRESS = '0x6C88E92b8Be2d0B6C10Ff2b1452213D008507eFd';
const FAUCET_URL = 'https://faucet.polygon.technology'
const METAMASK_URL = 'https://metamask.io/';


const App = () => {

  const [currentAccount, setCurrentAccount] = useState('');
  const [domain, setDomain] = useState('');
  const [record, setRecord] = useState('');
  const [network, setNetwork] = useState('');
  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [mints, setMints] = useState([]);

  const connectWallet = async ({ request=true }) => {
    if (!window.ethereum) {
      alert("Get MetaMask -> " + METAMASK_URL);
      return;
    }
    const method = request ? 'eth_requestAccounts' : 'eth_accounts';
    const accounts = await window.ethereum.request({ method: method });
    setCurrentAccount(accounts[0]);
    const chainId = await window.ethereum.request({ method: 'eth_chainId' });
    setNetwork(networks[chainId]);
    window.ethereum.on('chainChanged', handleChainChanged);
    function handleChainChanged(_chainId) {
      window.location.reload();
    }
  }
	
  const switchNetwork = async () => {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: '0x13881' }],
      });
    } catch (error) {
      if (error.code === 4902) {
        try {
          await window.ethereum.request({
            method: 'wallet_addEthereumChain',
            params: [
              {
                chainId: '0x13881',
                chainName: 'Polygon Mumbai Testnet',
                rpcUrls: ['https://rpc-mumbai.maticvigil.com/'],
                nativeCurrency: {
                  name: 'Mumbai Matic',
                  symbol: 'MATIC',
                  decimals: 18
                },
                blockExplorerUrls: ['https://mumbai.polygonscan.com/']
              }
            ]
          });
        } catch (error) {
          console.error(error);
        }
      }
      console.error(error);
    }
  }

  const mintDomain = async () => {
    if (!domain || !record) { return }
    setLoading(true);
    const price = domain.length === 3 ? '0.005' : domain.length === 4 ? '0.003' : '0.001';
    console.log('Minting domain', domain, 'with price', price);
    try {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(CONTRACT_ADDRESS, contractAbi.abi, signer);
      let tx = await contract.register(domain, {value: ethers.utils.parseEther(price)});
      const receipt = await tx.wait();
      if (receipt.status === 1) {
        console.log('Domain minted: https://mumbai.polygonscan.com/tx/'+tx.hash);
        console.log('Setting description next (second and last transaction)')
        tx = await contract.setRecord(domain, record);
        await tx.wait();
        console.log('Description set: https://mumbai.polygonscan.com/tx/'+tx.hash);
        setTimeout(() => { fetchMints() }, 2000);
        setRecord('');
        setDomain('');
      } else {
        alert('Transaction failed; please try again.');
      }
    } catch (error) {
      console.error(error);
    }
    setLoading(false);
  }

  const updateDomain = async () => {
    if (!record || !domain) { return }
    setLoading(true);
    try {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(CONTRACT_ADDRESS, contractAbi.abi, signer);
      let tx = await contract.setRecord(domain, record);
      await tx.wait();
      setTimeout(() => { fetchMints() }, 2000);
      setRecord('');
      setDomain('');
    } catch (error) {
      console.error(error);
    }
    setLoading(false);
    setEditing(false);
  }

  const fetchMints = async () => {
    try {
      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();
      const contract = new ethers.Contract(CONTRACT_ADDRESS, contractAbi.abi, signer);
      const names = await contract.getAllNames(); // name is the actual domain name
      const mintRecords = await Promise.all(names.map(async (name) => {
        const mintRecord = await contract.records(name); // this is the string description
        const owner = await contract.domains(name); // this is the address
        return {
          id: names.indexOf(name),
          name: name,
          record: mintRecord,
          owner: owner,
        };
      }));
      setMints(mintRecords);
    } catch (error) {
      console.error(error);
    }
  }

  const editRecord = (name) => {
    setEditing(true);
    setDomain(name);
    setRecord('');
  }

  const exitEditRecord = () => {
    setEditing(false);
    setDomain('');
    setRecord('');
  }

  const renderNotConnectedContainer = () => (
    <div className="connect-wallet-container">
      <button onClick={connectWallet} className="cta-button connect-wallet-button">
        Connect Wallet
      </button>
      <br />
      <img src="https://media1.giphy.com/media/fSaRxCT9ZbTRS/giphy.gif" alt="Brian gif" />
    </div>
  )

  const renderInputForm = () => {
    if (network !== 'Polygon Mumbai Testnet') {
      return (
        <div className="connect-wallet-container">
          <h2>The app runs on the Polygon Mumbai Testnet</h2>
          <button className='cta-button mint-button' onClick={switchNetwork}>
            Please click here to switch in Metamask
          </button>
        </div>
      );
    } else {
      return (
        <div className="form-container">
          <div className="first-row">
            <input
              type="text"
              value={domain}
              placeholder='domain'
              onChange={e => setDomain(e.target.value)}
              disabled={editing || loading}
            />
            <p className='tld'>{TLD}</p>
          </div>
          <input
            type="text"
            value={record}
            placeholder='description'
            onChange={e => setRecord(e.target.value)}
            disabled={loading}
          />
          {editing
            ? (
              <div className="button-container">
                <button className='cta-button mint-button' disabled={loading} onClick={updateDomain}>
                  Update
                </button>
                <button className='cta-button mint-button' disabled={loading} onClick={() => exitEditRecord()}>
                  Cancel
                </button>
              </div>
            )
            : (
              <div className="button=container">
                <button className='cta-button mint-button' disabled={loading} onClick={mintDomain}>
                  Mint
                </button>
                <p>This will pop two transactions: (1) Minting the domain (permanent) and (2) Writing its description (editable)</p>
                <p>Note: domain must be 3-10 characters</p>
              </div>
            )
          }
        </div>
      )
    }
  }

  const renderMints = () => {
    if (currentAccount && mints.length > 0) {
      return (
        <div className="mint-container">
          <p className="subtitle">Recently minted domains:</p>
          <div className="mint-list">
            {mints.map((mint, index) => (
              <div className="mint-item" key={index}>
                <div className="mint-row">
                  <a
                    className="link"
                    href={`https://testnets.opensea.io/assets/mumbai/${CONTRACT_ADDRESS}/${mint.id}`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <p className="underlined">
                      {mint.name}{TLD}
                    </p>
                  </a>
                  {mint.owner.toLowerCase() === currentAccount.toLowerCase() &&
                    <button
                      className="edit-button"
                      onClick={() => editRecord(mint.name)}
                      disabled={loading}
                    >
                      <img
                        className="edit-icon"
                        src="https://img.icons8.com/metro/26/000000/pencil.png"
                        alt="Edit button"
                      />
                    </button>
                  }
                </div>
                <p>{mint.record}</p>
              </div>
            ))}
          </div>
        </div>
      )
    }
  }

  useEffect(() => {
    connectWallet({ request: false });
  }, []);

  useEffect(() => {
    if (network === 'Polygon Mumbai Testnet') {
      fetchMints();
    }
  }, [currentAccount, network]);

  return (
    <div className="App">
      <div className="container">
        <div className="header-container">
          <header>
            <div className="left">
              <p className="title">Brian Name Service</p>
              <p className="subtitle">Mint your own domain</p>
            </div>
            <div className="right">
              <img alt="Network logo" className="logo" src={ network.includes("Polygon") ? polygonLogo : ethLogo} />
                {currentAccount
                  ? <p>Wallet: {currentAccount.slice(2, 6)}...{currentAccount.slice(-4)}</p>
                  : <p>Not connected</p>
                }
            </div>
          </header>
        </div>
        {currentAccount
          ? renderInputForm()
          : renderNotConnectedContainer()
        }
        {mints && renderMints()}
        <div className="footer-container">
          <a
            className="footer-text"
            href={FAUCET_URL}
            target="_blank"
            rel="noreferrer"
          >
            No MATIC? Click here for Polygon Mumbai Faucet
          </a>
        </div>
      </div>
    </div>
  );
}

export default App;
