const main = async () => {
  const domainContractFactory = await hre.ethers.getContractFactory('Domains');
  const domainContract = await domainContractFactory.deploy("brian");
  await domainContract.deployed();
  console.log("Contract deployed to:", domainContract.address);

  // let balance = await hre.ethers.provider.getBalance(domainContract.address);
  // console.log("Contract balance:", hre.ethers.utils.formatEther(balance));

  // let txn = await domainContract.register("supercontest", {value: hre.ethers.utils.parseEther('0.1')});
  // await txn.wait();
  // console.log("Minted domain supercontest.brian");

  // txn = await domainContract.setRecord("supercontest", "NFL wagers against lines");
  // await txn.wait();
  // console.log("Set record for supercontest.brian");

  // const address = await domainContract.getAddress("supercontest");
  // console.log("Owner of domain supercontest.brian:", address);

  // balance = await hre.ethers.provider.getBalance(domainContract.address);
  // console.log("Contract balance:", hre.ethers.utils.formatEther(balance));
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();