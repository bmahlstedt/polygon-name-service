// This current sequence shows the balance of the owner and the contract,
// runs a transaction, attempts a withdrawl as a non-owner (unsuccessfully),
// then withdraws the funds in the contract to the owner's address.

const main = async () => {
  const [owner, superCoder] = await hre.ethers.getSigners();
  const domainContractFactory = await hre.ethers.getContractFactory('Domains');
  const domainContract = await domainContractFactory.deploy("brian");
  await domainContract.deployed();
  console.log("Contract deployed to:", domainContract.address);
  console.log("Contract owner:", owner.address);

  let contractBalance = await hre.ethers.provider.getBalance(domainContract.address);
  console.log("Contract balance:", hre.ethers.utils.formatEther(contractBalance));
  let ownerBalance = await hre.ethers.provider.getBalance(owner.address);
  console.log("Owner balance:", hre.ethers.utils.formatEther(ownerBalance));

  let txn = await domainContract.register("mytest", {value: hre.ethers.utils.parseEther('0.1')});
  await txn.wait();

  contractBalance = await hre.ethers.provider.getBalance(domainContract.address);
  console.log("Contract balance:", hre.ethers.utils.formatEther(contractBalance));
  ownerBalance = await hre.ethers.provider.getBalance(owner.address);
  console.log("Owner balance:", hre.ethers.utils.formatEther(ownerBalance));

  try {
    txn = await domainContract.connect(superCoder).withdraw();
    await txn.wait();
  } catch (error) {
    console.log('Could not withdraw as a non-owner, good')
  }

  txn = await domainContract.connect(owner).withdraw();
  await txn.wait();

  contractBalance = await hre.ethers.provider.getBalance(domainContract.address);
  console.log("Contract balance:", hre.ethers.utils.formatEther(contractBalance));
  ownerBalance = await hre.ethers.provider.getBalance(owner.address);
  console.log("Owner balance:", hre.ethers.utils.formatEther(ownerBalance));
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();