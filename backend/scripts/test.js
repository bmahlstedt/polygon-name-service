const main = async () => {
  const domainContractFactory = await hre.ethers.getContractFactory('Domains');
  const domainContract = await domainContractFactory.deploy("brian");
  await domainContract.deployed();
  try {
    let txn = await domainContract.register("my", {value: hre.ethers.utils.parseEther('1')});
    await txn.wait();
  } catch (error) {
    console.log(error);
    console.log('This should have failed, domain is not between 3 and 10 chars.');
  }
  try {
    let txn = await domainContract.register("mylongdomain", {value: hre.ethers.utils.parseEther('1')});
    await txn.wait();
  } catch (error) {
    console.log(error);
    console.log('This should have failed, domain is not between 3 and 10 chars.');
  }
};

const runMain = async () => {
  try {
    await main();
    process.exit(0);
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

runMain();